package com.domain;

import java.time.LocalDate;

public class Actor extends Model{
    private String name;
    private LocalDate dateOfBirth;

    public void setName(String name) {
        this.name = name;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getName() {
        return name;

    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }


}
