package com.service;


import com.domain.Actor;
import com.mappers.ActorResultMapper;

public class RepositoryCatalog {
    public Repository<Actor> actorRepository(){
        return new ActorManager(new ActorResultMapper());
    }
}
