package com.service;

import com.domain.Season;
import com.mappers.ResultSetMapper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SeasonManager extends RepositoryBase {
    EpisodeManager episodeManager;

    private String createTable = "CREATE TABLE IF NOT EXISTS Season(id int primary key auto_increment, seasonNumber int, yearOfRelease datetime, episodes text)";


    public SeasonManager(ResultSetMapper<Season> mapper) {
        super(mapper);
        episodeManager = new EpisodeManager();
    }

    public int addSeason(Season season) {
        int count = 0;
        try {
            addSql.setInt(1, season.getSeasonNumber());
            addSql.setInt(2, season.getYearOfRelease());

            count = addSql.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    public List<Season> get() {
        List<Season> seasons = new ArrayList<Season>();

        try {
            ResultSet rs = getSql.executeQuery();

            while (rs.next()) {
                Season p = new Season();

                seasons.add(p);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return seasons;
    }

    protected String addSqlStmt() {
        return "INSERT INTO Season (seasonNumber, yearsOfRelease, episodes) VALUES (?, ?)";
    }

    protected String deleteSqlStmt() {
        return "DELETE FROM Season";
    }

    protected String getSqlStmt() {
        return "SELECT id, name, seasonNumber, duration FROM Season";
    }

    protected String type() {
        return "Season";
    }
}
