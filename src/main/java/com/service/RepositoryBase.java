package com.service;
import com.mappers.ResultSetMapper;
import java.util.List;
import java.sql.*;

public abstract class RepositoryBase<TEntity> implements Repository<TEntity> {
    protected Connection connection;
    protected String url = "jdbc:hsqldb:hsql://localhost/workdb";
    protected PreparedStatement addSql;
    protected PreparedStatement deleteSql;
    protected PreparedStatement getSql;
    protected Statement statement;
    protected String createTable;
    ResultSetMapper<TEntity> mapper;

    protected RepositoryBase(ResultSetMapper<TEntity> mapper)  {
        this.mapper = mapper;
        try {
            connection = DriverManager.getConnection(url);
            statement = connection.createStatement();

            ResultSet rs = connection.getMetaData().getTables(null, null, null,
                    null);
            boolean tableExists = false;
            while (rs.next()) {
                if (type().equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
                    tableExists = true;
                    break;
                }
            }

            if (!tableExists)
                statement.executeUpdate(createTable);

            addSql = connection
                    .prepareStatement(addSqlStmt());
            deleteSql = connection
                    .prepareStatement(deleteSqlStmt());
            getSql = connection
                    .prepareStatement(getSqlStmt());

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    protected Connection getConnection() {
        return connection;
    }
    public void clear() {
        try {
            deleteSql.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public List<TEntity> get(){
        List<TEntity> result = new ArrayList<TEntity>();
        try {
            ResultSet rs = getSql.executeQuery();
            while (rs.next()) {
                result.add(mapper.map(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }


    protected abstract String addSqlStmt();

    protected abstract String deleteSqlStmt();

    protected abstract String getSqlStmt();

    protected abstract String type();
}
