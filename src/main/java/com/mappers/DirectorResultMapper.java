package com.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.domain.Director;

public class DirectorResultMapper  implements ResultSetMapper<Director>{

    public Director map(ResultSet rs) throws SQLException {
        Director p = new Director();
        p.setId(rs.getInt("id"));
        p.setName(rs.getString("name"));
        p.setDateOfBirth(rs.getDate("dateOfBirth").toLocalDate());
        p.setBiography(rs.getString("biography"));
        return p;
    }
}
