package com.mappers;
import com.domain.Episode;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalTime;

public class EpisodeResultMapper  implements ResultSetMapper<Episode>{

    public Episode map(ResultSet rs) throws SQLException {
        Episode p = new Episode();
        p.setId(rs.getInt("id"));
        p.setName(rs.getString("name"));
        p.setReleaseDate(rs.getDate("releaseDate").toLocalDate());
        p.setEpisodeNumber(rs.getInt("episodeNumber"));
        p.setDuration(Duration.between(LocalTime.MIDNIGHT, rs.getTime("duration").toLocalTime()));
        return p;
    }
}
